# Comandi Git Gabriele Feroglia

## Inizializzazione e configurazione

- `git init`: Inizializza un nuovo repository Git nella directory corrente.
- `git clone [url]`: Clona un repository Git esistente da un URL remoto nella tua directory locale.
- `git config`: Configura le opzioni di Git, come il nome utente e l'indirizzo email.

## Gestione dei cambiamenti

- `git add [file]`: Aggiunge un file specifico o un insieme di file alla staged area in preparazione per il commit.
- `git commit -m "[messaggio]"`: Esegue un commit dei cambiamenti nella staged area con un messaggio descrittivo.
- `git diff`: Mostra le modifiche non ancora stage tra il repository e la working directory.
- `git restore [file]`: Ripristina un file dalla sua ultima versione salvata nel repository.
- `git stash`: Nasconde le modifiche non commit in una "stash" temporanea.
- `git bisect`: Aiuta a trovare il commit che ha introdotto un determinato bug.

## Branching e merging

- `git branch`: Mostra l'elenco dei branch locali e corrente.
- `git branch [nome_branch]`: Crea un nuovo branch con il nome specificato.
- `git checkout [nome_branch]`: Passa a un branch esistente o creato di recente.
- `git checkout -b [nome_branch]`: Crea un nuovo branch e passa ad esso in un unico comando.
- `git merge [nome_branch]`: Esegue il merge delle modifiche di un branch specifico nel branch corrente.
- `git rebase [nome_branch]`: Ricostruisce la cronologia del branch corrente sul branch specificato.
- `git cherry-pick [commit]`: Applica le modifiche di un commit specifico su un altro branch.

## Collaborazione remota

- `git remote add [nome_remoto] [url_remoto]`: Aggiunge un repository remoto con un nome specificato.
- `git push [nome_remoto] [nome_branch]`: Carica i commit nel repository remoto specificato.
- `git pull [nome_remoto] [nome_branch]`: Esegue il fetch dei cambiamenti dal repository remoto e li merge nel branch corrente.
- `git fetch`: Recupera tutti i riferimenti remoti dal repository remoto specificato senza fare il merge.

## Stato e cronologia

- `git status`: Mostra lo stato attuale del repository, inclusi i file modificati, i file nella staged area e altro.
- `git log`: Mostra la cronologia dei commit nel repository.
- `git show [commit]`: Mostra le modifiche apportate da un commit specifico.
- `git blame [file]`: Mostra l'ultima modifica di ogni linea di un file specifico.




